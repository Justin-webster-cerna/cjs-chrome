function RGBToHex(rgb) {
    // Choose correct separator
    let sep = rgb.indexOf(",") > -1 ? "," : " ";
    
    // Turn "rgb(r,g,b)" into [r,g,b]
    rgb = rgb.substr(4).split(")")[0].split(sep);
  
    let r = (+rgb[0]).toString(16),
        g = (+rgb[1]).toString(16),
        b = (+rgb[2]).toString(16);
  
    if (r.length == 1)
      r = "0" + r;
    if (g.length == 1)
      g = "0" + g;
    if (b.length == 1)
      b = "0" + b;
  
    return r + g + b;
}

function setTabIcon() {
    var el = document.getElementsByClassName('navpage-header')[0] || document.getElementById('chrome-envmarker'),
        color = RGBToHex(window.getComputedStyle(el, null).getPropertyValue("background-color")),
        link = document.querySelector("link[rel*='icon']") || document.createElement('link');
    
    link.type = 'image/x-icon';
    link.rel = 'shortcut icon';
    link.href = 'https://via.placeholder.com/64/' + color + '/' + color;

    document.getElementsByTagName('head')[0].appendChild(link);
}